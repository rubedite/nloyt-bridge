const http = require('http');
const WebSocket = require('ws');
const url = require('url');

const randString = (num) => {
  if (!num) num = 30;
  return Math.random().toString(36).substring(2, Math.floor(num / 2)) +
    Math.random().toString(36).substring(2, Math.floor(num / 2)) +
    (Math.round(new Date().getTime() / 1000));
}
function heartbeat() {
  this.isAlive = true;
}
function noop() {}


const webservers = {};
const logs = {};
const requestHandler = (request, response) => {
  const server = randString();
  if (request.url.indexOf('/stop') > -1) {
    const serverId = request.url.replace('/stop/', '');
    if (webservers[serverId]) {
      const result = logs[serverId];
      webservers[serverId].close();
      delete webservers[serverId];
      delete logs[serverId];
      response.end('' + result + "\n")
    } else {
      response.end('empty logs' + "\n")
    }

  } else if (request.url === '/start') {

    (function (server) {

      webservers[server] = new WebSocket.Server({noServer: true});
      logs[server] = '';
      webservers[server].on('connection', function connection(ws, req) {
        ws.isAlive = true;
        ws.on('pong', heartbeat);

        ws.on('message', function incoming(message) {
          logs[server] += message + "\r\n";
          if (message === 'ping') {
            ws.send('pong');
          } else if (message === 'stop') {
            webservers[server].close();
            delete webservers[server];
          } else {
            webservers[server].clients.forEach(function each(wsc) {
              if (wsc.readyState === WebSocket.OPEN && wsc !== ws) {
                wsc.send(message);
              }
            })
          }
        });

        ws.send('hello');
      });

      const interval = setInterval(function ping() {
        let active = false;
        webservers[server].clients.forEach(function each(ws) {
          if (ws.isAlive === false) return ws.terminate();

          active = true;
          ws.isAlive = false;
          ws.ping(noop);
        });

        if (!active) {
          console.log('terminate ' + server + ' by timeout');
          webservers[server].close();
          delete webservers[server];
        }
      }, 20000);

      webservers[server].on('close', function close() {
        clearInterval(interval);
      });
    })(server);

    console.log('server started: ' + server);
    response.end('' + server + "\n")
  } else {
    response.end('Bad request');
  }
}

const server = http.createServer(requestHandler);

server.on('upgrade', function upgrade(request, socket, head) {
  let pathname = url.parse(request.url).pathname;
  pathname = pathname.substring(1)
  if (webservers[pathname]) {

    webservers[pathname].handleUpgrade(request, socket, head, function done(ws) {
      webservers[pathname].emit('connection', ws, request);
    });
  } else {
    socket.destroy();
  }
});

server.listen(8888);
// server.listen(process.env.PORT);
